﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rutas
{
    public static List<int> RutaMasCorta(GraphObject graph, int origen, int destino)
    {
        
        // Chequear si origen o destino tienen vecinos
        if(graph.nodoObject[origen].indiceVecinos.Count < 1 || graph.nodoObject[destino].indiceVecinos.Count < 1)
        {
            Debug.Log("No hay conexion entre " + origen + " y " + destino);
            return null;
        }

        List<int> ruta = new List<int>();

        float distanciaRecorrida;

        int cantNodos = graph.CantidadDeNodos;
        int nodoActual = origen;
        
        TablaDeRecorrido[] tabla = new TablaDeRecorrido[cantNodos];

        //TABLA DE RECORRIDO
        // tabla.visitado       [false = noVisitado , true = visitado]
        // tabla.distancia      [cantidad de nodos recorridos]
        // tabla.indicePrevio   [index de nodo anterior]

        for (int n = 0; n < cantNodos; n++)
        {
            tabla[n].visitado       = false;
            tabla[n].distancia      = float.MaxValue;
            tabla[n].indicePrevio   = 0;
        }

        tabla[origen].distancia = 0;

        while(nodoActual != -1)
        {
            tabla[nodoActual].visitado = true;
            for (int col = 0; col < cantNodos; col++)
            {
                // si hay distancia entre el actual y el consultado ejecuta el if
                if(graph.GetNodeDistance(nodoActual,col) != 0)
                {
                    //calcular distancia
                    distanciaRecorrida = graph.GetNodeDistance(nodoActual, col) + tabla[nodoActual].distancia;

                    //colocar distancia en tabla
                    if(distanciaRecorrida < tabla[col].distancia)
                    {
                        tabla[col].distancia = distanciaRecorrida;  //actualizar distancia
                        tabla[col].indicePrevio = nodoActual;       //actualizar nodo previo
                    }
                }
            }

            int indiceMenor = -1;
            float distanciaMenor = float.MaxValue;

            for (int x = 0; x < cantNodos; x++)
            {
                if(tabla[x].distancia < distanciaMenor && !tabla[x].visitado)
                {
                    indiceMenor = x;
                    distanciaMenor = tabla[x].distancia;
                }
            }

            nodoActual = indiceMenor;
        }

        int nodoFinal = destino;

        while(nodoFinal != origen)
        {
            ruta.Add(nodoFinal);
            nodoFinal = tabla[nodoFinal].indicePrevio;
        }

        ruta.Add(origen);
        ruta.Reverse();

        // imprimir ruta
        string rutasIndex = "La ruta entre "+ origen.ToString() + " y "+destino.ToString() +" pasa por ";
        for (int i = 0; i < ruta.Count; i++)
        {
            rutasIndex += ">"+ ruta[i].ToString();           
        }
        Debug.Log(rutasIndex);

        return ruta;
    }

    public struct TablaDeRecorrido
    {
        public bool visitado;
        public float distancia;
        public int indicePrevio;

        public TablaDeRecorrido(bool visitado, float distancia, int indicePrevio)
        {
            this.visitado = visitado;
            this.distancia = distancia;
            this.indicePrevio = indicePrevio;
        }
    }
}
