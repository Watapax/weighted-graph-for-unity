﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;


public enum NodoAction { Walk, Jump, Dash }

[System.Serializable]
public class Nodo
{
    public Vector3 poposicionNodo;
    public List<int> indiceVecinos = new List<int>();
    public NodoAction nodoAction = NodoAction.Walk;

    public float Distancia(Vector3 posicionVecino)
    {
        float w = Vector3.Distance(poposicionNodo , posicionVecino);
        return w;
    }

    public Nodo(Vector3 _pos, List<int> _indiceVecinos)
    {
        poposicionNodo = _pos;
        indiceVecinos.Clear();
        indiceVecinos = _indiceVecinos;
    }
}

//public struct AdyacentMatrix
//{
//    public float[,] matrix;
//    public AdyacentMatrix( int _nodesSize)
//    {
//        matrix = new float[_nodesSize, _nodesSize];
       
//    }
//}

[CreateAssetMenu]
public class GraphObject : ScriptableObject
{

    public float[,] adyacentMatrix;
    //public AdyacentMatrix _adyacentMatrix;
    public List<Nodo> nodoObject = new List<Nodo>();
    public int nodos;

    public void Crear()
    {
        //Crear el Graph si es que la matriz está vacia

        nodos = nodoObject.Count;
        //_adyacentMatrix = new AdyacentMatrix(nodos);
        adyacentMatrix = new float[nodos, nodos];

        for(int x = 0; x < nodos; x++)
        {
            for(int y = 0; y < nodos; y++)
            {
                adyacentMatrix[x, y] = float.MaxValue;
            }
        }


        for (int i = 0; i < nodos; i++)
        {
            int vecinos = nodoObject[i].indiceVecinos.Count;

            for (int x = 0; x < vecinos; x++)
            {
                int indexNodoVecino = nodoObject[i].indiceVecinos[x];
                Vector3 posicionVecino = nodoObject[indexNodoVecino].poposicionNodo;
                float distancia = nodoObject[i].Distancia(posicionVecino);

                AgregarNodoMatrix(i, indexNodoVecino, distancia);
            }
        }
       // Debug.Log(adyacentMatrix[1, 3] + " matrix");
    }


    public void AgregarNodo(Vector3 nodePos, List<int> _indiceVecinos)
    {
        Nodo nodoTemp = new Nodo(nodePos, _indiceVecinos);
        nodoObject.Add(nodoTemp);
    }

    void AgregarNodoMatrix(int startNode, int endNode, float distance)
    {
        //_adyacentMatrix.matrix[startNode, endNode] = distance;
        adyacentMatrix[startNode, endNode] = distance;

    }

    public float GetNodeDistance(int row, int col)
    {
        //return _adyacentMatrix.matrix[row, col];
        return adyacentMatrix[row, col];
    }

    public int CantidadDeNodos
    {
        get { return nodos; }
    }

    public void ClearMatrix()
    {
        for (int i = 0; i < nodos; i++)
        {
            for (int x = 0; x < nodos; x++)
            {
                //_adyacentMatrix.matrix[i, x] = 0;
                adyacentMatrix[i, x] = 0;
            }
        }

    }

    public void ClearNodos()
    {
        nodoObject.Clear();
    }

    public void ImprimirMatrix()
    {
        for(int i = 0; i < CantidadDeNodos; i++)
        {
            for (int x = 0; x < CantidadDeNodos; x++)
            {
              //  Debug.Log(_adyacentMatrix.matrix[i, x]);
            }
        }
    }

}
