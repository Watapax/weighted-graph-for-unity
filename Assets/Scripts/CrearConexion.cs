﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrearConexion : MonoBehaviour
{
    LineRenderer lineRenderer;
    Camera mainCamera;
    Node node;

    private void Awake()
    {
        node = GetComponentInParent<Node>();
        lineRenderer = GetComponentInChildren<LineRenderer>();
        lineRenderer.enabled = false;
        mainCamera = Camera.main;
    }

    private void OnMouseDown()
    {
        lineRenderer.enabled = true;      
        node.SetNodoA();
    }


    void MostrarTempConexion()
    {

        Vector2 mousePos = Input.mousePosition;
        Vector3 mouseWorldPos = mainCamera.ScreenToWorldPoint(mousePos);
        mouseWorldPos.z = -.1f;
        Vector3 posicion0 = node.transform.position;
        posicion0.z = -.1f;
    
        lineRenderer.SetPosition(0, posicion0);
        lineRenderer.SetPosition(1, mouseWorldPos);
    }

    private void Update()
    {
        if(lineRenderer.enabled)
        {
            MostrarTempConexion();
        }

        if(Input.GetMouseButtonUp(0)&& lineRenderer.enabled)
        {
            lineRenderer.enabled = false;           
        }
    }

}
