﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuscarRutas : MonoBehaviour
{
    public GraphObject graph;
    public LineRenderer lineaRuta;
    public Dropdown desde, hacia;

    Vector3 offsetTexture = Vector3.zero;

    int prevNodosCount;

    private void Start()
    {
        lineaRuta.enabled = false;
        PoblarDropdown();
    }

    // esto es opcional si se busca la ruta desde otro metodo
    public void PoblarDropdown()
    {
        desde.ClearOptions();
        hacia.ClearOptions();

        for (int i = 0; i < graph.nodoObject.Count; i++)
        {
            Dropdown.OptionData optionData = new Dropdown.OptionData(i.ToString());            
            desde.options.Add(optionData);
            hacia.options.Add(optionData);
        }

        prevNodosCount = graph.nodoObject.Count;
       

    }

    private void Update()
    {
        // visualizar la ruta moviendo la textura del lineRenderer
        if(lineaRuta.enabled)
        {
            offsetTexture.x -= Time.deltaTime * 5;
            lineaRuta.sharedMaterial.SetTextureOffset("_MainTex", offsetTexture);
        }

        // apago el lineRenderer si apreto el mouse, para prevenir que se siga dibujando si se movio algun nodo
        if (Input.GetMouseButtonDown(0))
            lineaRuta.enabled = false;

        // actualiza los datos de los dropDown de la UI si se modifico el graph
        if(prevNodosCount != graph.nodoObject.Count)
        {
            PoblarDropdown();
        }
    }

    // este metodo lo llamo desde el boton
    public void EncontrarRuta()
    {
        // definir indices de origen y destino
        int origen = desde.value;
        int destino = hacia.value;

        // lista que almacenará los indices de cada nodo de la ruta
        List<int> indexRuta = new List<int>();

        // Aca buscamos la ruta, esta linea es basicamente todo lo que hay que hacer, el resto es adorno
        indexRuta = Rutas.RutaMasCorta(graph, origen, destino);

        // si no hay ruta, a la verga todo..
        if (indexRuta == null) return;

        // visualizar ruta con un lineRenderer
        lineaRuta.positionCount = indexRuta.Count;       
        for(int i = 0; i < indexRuta.Count; i++)
        {
            lineaRuta.SetPosition(i, graph.nodoObject[indexRuta[i]].poposicionNodo);
        }
        lineaRuta.enabled = true;
        

       
    }
}
