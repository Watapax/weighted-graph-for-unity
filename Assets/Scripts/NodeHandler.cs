﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class NodeHandler : MonoBehaviour
{
    Vector3 offset;
    Camera mainCamera;
    public GameObject borrarNodo, crearConexion;
    public SpriteRenderer circulo;
    Color normalColor = Color.grey;
    public Color hoverColor = Color.cyan;
    bool optionsVisible;
    Node node;
    Vector3 startSize;
    Vector3 hoverSize;
    public GameObject nodeActionsPanel;

    private void Awake()
    {
        mainCamera = Camera.main;
        node = GetComponent<Node>();
        startSize = Vector3.one;
        hoverSize = startSize * 1.2f;
        nodeActionsPanel.SetActive(false);
       // borrarNodo.SetActive(false);
       // crearConexion.SetActive(false);

    }



    private void OnMouseOver()
    {
        circulo.transform.localScale = hoverSize;
        circulo.color = hoverColor;
        if (Input.GetMouseButtonUp(1) )
        {
            nodeActionsPanel.SetActive(true);
            
            //if(!optionsVisible)
            //{
            //    ToggleOptions(true);
            //    optionsVisible = true;
            //}
            //else
            //{
            //    ToggleOptions(false);
            //    optionsVisible = false;
            //}

        }

        if(Input.GetMouseButtonUp(0))
        {
            
            node._graphCreator.SetNodoB(node);
            node._graphCreator.CrearConexion();
        }
    }

    private void OnMouseExit()
    {
        circulo.transform.localScale = startSize;
        circulo.color = normalColor;
    }

    private void ToggleOptions(bool _state)
    {
        borrarNodo.SetActive(_state);
        crearConexion.SetActive(_state);
    }

    private void OnMouseDown()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos = mainCamera.ScreenToWorldPoint(mousePos);
        mousePos.z = 0;
        offset = transform.position - mousePos;
    }

    private void OnMouseUp()
    {
        node._graphCreator.SetGraphData();

    }

    private void OnMouseDrag()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos = mainCamera.ScreenToWorldPoint(mousePos);
        mousePos.z = 0;
        transform.position = mousePos + offset;

    }
}
