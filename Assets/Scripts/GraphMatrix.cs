﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GraphMatrix : ScriptableObject
{
    [SerializeField]
    public float[,] adyacentMatrix;
}
