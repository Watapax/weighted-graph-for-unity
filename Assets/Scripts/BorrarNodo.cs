﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorrarNodo : MonoBehaviour
{
    Node node;

    private void Awake()
    {
        node = GetComponentInParent<Node>();
    }

    private void OnMouseDown()
    {
        print("borrar nodo");
        node.BorrarNodo();
    }


}
