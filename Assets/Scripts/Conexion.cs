﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conexion : MonoBehaviour
{
    LineRenderer lineRenderer;
    Transform pointA, pointB;

    void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    public void SetPositions(Transform _pointA, Transform _pointB)
    {
        pointA = _pointA;
        pointB = _pointB;
    }

    bool IsSet => (pointA != null && pointB != null);

    void Update()
    {
        if(IsSet)
        {
            lineRenderer.SetPosition(0, pointA.position);
            lineRenderer.SetPosition(1, pointB.position);
        }
    }
}
