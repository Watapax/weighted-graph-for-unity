﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Conexiones
{
    public Transform rootNode;
    public int[] connectedNodes;

    public float Weight(Transform vecino)
    {
        float w = Vector3.Distance(rootNode.position, vecino.position);
        return w;
    }
}

public class ElGrafo : MonoBehaviour
{

    public static ElGrafo instance;
    public InputField desde,hacia;
    public Transform[] nodosTransform;
    public Conexiones[] conexiones;
    public int inicio;
    public int final;
    float distancia;
    int n = 0;
    int m = 0;
    int cantNodos = 8;
    string dato = "";
    int actual;
    int columna;

    LineRenderer lineRuta;

    Graph graph = new Graph(8);
    int nodo = 0;



    //private void Awake()
    //{
    //    instance = this;
    //    AlimentarWeightedGraph();
    //    lineRuta = GetComponent<LineRenderer>();
    //    lineRuta.enabled = false;
    //}


    //void AlimentarWeightedGraph()
    //{
    //    for(int i = 0; i < conexiones.Length; i++)
    //    {
    //        for(int n = 0; n < conexiones[i].connectedNodes.Length; n++)
    //        {
    //            graph.AdicionarArista(i, conexiones[i].connectedNodes[n], conexiones[i].Weight(nodosTransform[conexiones[i].connectedNodes[n]]));
    //        }
    //    }
    //}

    //public void RecalcularGraph()
    //{
    //    graph.ClearGraph();
    //    AlimentarWeightedGraph();
    //}


    public void EjecutarDiskstra()
    {
        inicio = int.Parse(desde.text);
        final = int.Parse(hacia.text);
        Dijkstra();
    }

    public void Dijkstra()
    {
        float[,] tabla = new float[cantNodos, 3]; 
        //TABLA DE RECORRIDO
        // 0 - Visitado     [0 = noVisitado , 1= visitado]
        // 1 - Distancia    [cantidad de nodos recorridos]
        // 2 - Previo       [index de nodo anterior]

        for (int n = 0; n < cantNodos; n++)
        {
            tabla[n, 0] = 0;
            tabla[n, 1] = int.MaxValue;
            tabla[n, 2] = 0;
        }
        tabla[inicio, 1] = 0;

        actual = inicio;

        do
        {
            // marcar nodo visitado
            tabla[actual, 0] = 1;
            for (columna = 0; columna < cantNodos; columna++)
            {
                if (graph.ObtenerAdyacencia(actual, columna) != 0)
                {
                    //calcular distancia
                    distancia = graph.ObtenerAdyacencia(actual, columna) + tabla[actual, 1];

                    //colocar distancia
                    if (distancia < tabla[columna, 1])
                    {
                        tabla[columna, 1] = distancia;  //actualizar distancia
                        tabla[columna, 2] = actual;     //actualizar nodo previo
                    }
                }
            }
            // actualizar actual , si tiene menor distancia
            int indiceMenor = -1;
            float distanciaMenor = int.MaxValue;

            for(int x = 0; x < cantNodos; x++)
            {
                if(tabla[x,1] < distanciaMenor && tabla[x,0]==0)
                {
                    indiceMenor = x;
                    distanciaMenor = tabla[x, 1];
                }
            }

            actual = indiceMenor;

        } while (actual != -1);

        //Obtener la ruta
        List<int> ruta = new List<int>();
        int nodo = final;

        while(nodo != inicio)
        {
            ruta.Add(nodo);
            nodo = (int)tabla[nodo, 2];
        }
        ruta.Add(inicio);
        ruta.Reverse();

        foreach(int posicion in ruta)
        {
            Debug.Log(" -> " + posicion);
        }

        //DibujarRuta(ruta);

    }

    //public void OcultarRuta()
    //{
    //    lineRuta.enabled = false;
    //}

    //void DibujarRuta(List<int> _ruta)
    //{
    //    lineRuta.positionCount = _ruta.Count;
    //    for(int i = 0; i < _ruta.Count; i++)
    //    {
    //        lineRuta.SetPosition(i, nodosTransform[_ruta[i]].position);
    //    }
    //    lineRuta.enabled = true;
    //}

    //private void Update()
    //{
    //    if(lineRuta.enabled)
    //    {
    //        offsetTexture.x -= Time.deltaTime * speedLine;
    //        materialLinea.SetTextureOffset("_MainTex", offsetTexture);
    //    }
    //}

    //public Material materialLinea;
    //Vector2 offsetTexture;
    //float speedLine = 5;
}
