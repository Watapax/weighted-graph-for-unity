﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragNode : MonoBehaviour
{
    Vector3 offset;
    Camera mainCamera;

    private void Awake()
    {
        mainCamera = Camera.main;
    }
    private void OnMouseDown()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos = mainCamera.ScreenToWorldPoint(mousePos);
        mousePos.z = 0;
        offset = transform.position - mousePos;
      //  ElGrafo.instance.OcultarRuta();
    }

    private void OnMouseDrag()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos = mainCamera.ScreenToWorldPoint(mousePos);
        mousePos.z = 0;
        transform.position = mousePos + offset;

    }

    private void OnMouseUp()
    {
        //ElGrafo.instance.RecalcularGraph();
    }
}
