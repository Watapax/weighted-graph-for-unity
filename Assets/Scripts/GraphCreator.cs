﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GraphCreator : MonoBehaviour
{
    public GraphObject graph;
    public GameObject nodePrefab;
    public GameObject edgePrefab;
   // public BuscarRutas buscarRutas;

    List<Node> Nodos = new List<Node>();
    Camera mainCamera;
    Node nodoA, nodoB; // se usan para conectar 2 nodos

    public void SetNodoA(Node value)
    { nodoA = value; }

    public void SetNodoB(Node value)
    { nodoB = value; }

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    private void Start()
    {
        VisualizarGraphOnStart();
        SetGraphData();
    }

    
    public void AgregarNodo()
    {
        Vector3 nodePos = mainCamera.ViewportToWorldPoint(Vector2.one * .5f);
        nodePos.z = 0;

        GameObject go = (GameObject)Instantiate(nodePrefab, nodePos, Quaternion.identity);      
        go.transform.parent = transform;


        Node _node = go.GetComponent<Node>();
        _node._graphCreator = this;

        Nodos.Add(_node);
        _node.indice = Nodos.Count - 1;

        SetGraphData();
    }


    public void EliminarNodo(int index)
    {
        Nodos.RemoveAt(index);
        ReordenarNodos();

    }

    public void ReordenarNodos()
    {
        for (int i = 0; i < Nodos.Count; i++)
        {
            Nodos[i].indice = i;
        }
        
    }

    public void CrearConexion()
    {
        if(nodoA!= null && nodoB != null)
        {
            if(nodoA != nodoB)
            {
                if (!nodoA.IsConnected(nodoB))
                {
                    Conexion();
                }
                else 
                {
                    int vecinoA = nodoB.GetVecinoIndex(nodoA);
                    int vecinoB = nodoA.GetVecinoIndex(nodoB);

                    nodoA.RemoverConexion(vecinoB);
                    nodoB.RemoverConexion(vecinoA);

                    nodoA.RemoverVecino(vecinoB);
                    nodoB.RemoverVecino(vecinoA);
                    
                }
            }
            
        }

        nodoA = null;
        nodoB = null;

        SetGraphData();
    }


    void Conexion()
    {
        GameObject edgeObject = (GameObject)Instantiate(edgePrefab);
        edgeObject.transform.parent = transform;

        Conexion conexion = edgeObject.GetComponent<Conexion>();
        conexion.SetPositions(nodoA.transform, nodoB.transform);

        nodoA.AgregarVecino(nodoB, conexion);
        nodoB.AgregarVecino(nodoA, conexion);
    }

    public void ActualizarDistanciaEntreNodos()
    {
        for (int i = 0; i < Nodos.Count; i++)
        {
            Nodos[i].ActualizarDistancia();
        }
    }

    public void SetGraphData()
    {
        graph.ClearNodos();

        for (int i = 0; i < Nodos.Count; i++)
        {
            Vector3 nodePos = Nodos[i].GetPosition;
            List<int> indiceVecinos = Nodos[i].GetIndexVecinos;
            graph.AgregarNodo(nodePos, indiceVecinos);
        }

        CrearGraph();

    }

    public void CrearGraph()
    {
        graph.Crear();
    }

    public void VisualizarGraphOnStart()
    {
        if (graph == null) return;
        if (graph.nodoObject.Count == 0) return;
        //if(graph.adyacentMatrix.Length < 1) return;

        // instanciar Nodos
        for (int i = 0; i < graph.nodoObject.Count; i++)
        {
            Vector3 pos = graph.nodoObject[i].poposicionNodo;
            GameObject go = (GameObject)Instantiate(nodePrefab, pos, Quaternion.identity);
            go.transform.parent = transform;


            Node _node = go.GetComponent<Node>();
            _node._graphCreator = this;

            Nodos.Add(_node);
            _node.indice = Nodos.Count - 1;
        }

        // Agregarle los vecinos
        for(int i = 0; i < graph.nodoObject.Count; i++)
        {
            for(int x = 0; x < graph.nodoObject[i].indiceVecinos.Count; x++)
            {
                nodoA = Nodos[i];
                nodoB = Nodos[graph.nodoObject[i].indiceVecinos[x]];
                if (!nodoA.IsConnected(nodoB))
                    Conexion();
            }
        }

        nodoA = null;
        nodoB = null;

    }

}
