﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Edge : MonoBehaviour
{
    public Transform nodeA, nodeB;
    LineRenderer lineRenderer;
    public TextMesh dist;
    Vector3 distPos;
    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }



    private void Update()
    {
        if(nodeA!= null && nodeB!= null)
        {
            lineRenderer.SetPosition(0, nodeA.position);
            lineRenderer.SetPosition(1, nodeB.position);
            dist.text = Vector2.Distance(nodeA.position, nodeB.position).ToString(".0");
            dist.transform.position = (nodeA.position + nodeB.position) / 2;            
        }

    }
}
