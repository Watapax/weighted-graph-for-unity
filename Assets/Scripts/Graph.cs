﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph
{
    float[,] mAdyacencia;
    int nodos;

    public Graph(int pNodos)
    {
        nodos = pNodos;
        mAdyacencia = new float[nodos, nodos];
    }



    public void AdicionarArista(int pNodoInicio, int pNodoFinal, float pPeso)
    {
        mAdyacencia[pNodoInicio, pNodoFinal] = (int)pPeso;
    }


    public float ObtenerAdyacencia(int pfila, int pColumna)
    {
        return mAdyacencia[pfila, pColumna];
    }

    public void ClearGraph()
    {
        for(int i = 0; i < nodos; i++)
        {
            for (int x = 0; x < nodos; x++)
            {
                mAdyacencia[i, x] = 0;
            }
        }
    }

}
