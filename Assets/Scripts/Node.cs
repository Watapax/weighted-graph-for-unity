﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Node : MonoBehaviour
{



    public struct NodoVecino
    {
        public float distancia;
        public Node nodoVecino;
        public Conexion conexion;       

        public NodoVecino(float _distancia, Node _nodoVecino, Conexion _conexion)
        {
            distancia = _distancia;
            nodoVecino = _nodoVecino;
            conexion = _conexion;
        }

        public void ActualizarDistancia(float _distancia)
        {
            distancia = _distancia;
        }
    }

    [SerializeField] public List<NodoVecino> nodosVecinos = new List<NodoVecino>();
     public NodoAction nodoAction;
     public GraphCreator _graphCreator;
    public GameObject nodoActionBotonPrefab;
    public Transform panelTransform;

    public TextMeshPro indexNodoText;

    public int indice;

    public void BorrarNodo()
    {
        print("Borrando");
        for(int i = 0; i< nodosVecinos.Count; i++)
        {
            for (int x = 0; x < nodosVecinos[i].nodoVecino.nodosVecinos.Count; x++)
            {
                if(nodosVecinos[i].nodoVecino.nodosVecinos[x].nodoVecino == this)
                {
                    Destroy(nodosVecinos[i].conexion.gameObject); //nodoVecino.nodosVecinos[x].conexion.gameObject);
                    nodosVecinos[i].nodoVecino.nodosVecinos.RemoveAt(x);
                    goto outerLoop;
                }
            }

            outerLoop:
            continue;
        }

        _graphCreator.EliminarNodo(indice);
       
        Destroy(gameObject);
    }

    public void AgregarVecino(Node _nodoVecino, Conexion _conexion)
    {
        float distancia = Vector3.Distance(transform.position, _nodoVecino.transform.position);
        NodoVecino nodoVecino = new NodoVecino(distancia, _nodoVecino, _conexion);
        nodosVecinos.Add(nodoVecino);
    }

    public int GetVecinoIndex(Node _vecino)
    {
        int aux = 0;

        for(int i = 0; i < nodosVecinos.Count;i++)
        {
            if(nodosVecinos[i].nodoVecino == _vecino)
            {
                aux = i;
                break;
            }
        }

        return aux;
    }

    public void RemoverConexion(int _index)
    {
        Destroy(nodosVecinos[_index].conexion.gameObject);
    }

    public void RemoverVecino(int _index)
    {
        nodosVecinos.RemoveAt(_index);
    }

    public Vector3 GetPosition => transform.position;
    
    public List<int> GetIndexVecinos
    {
        get
        {
            List<int> tempList = new List<int>();
            for (int i = 0; i < nodosVecinos.Count; i++)
            {
                tempList.Add(nodosVecinos[i].nodoVecino.indice);
            }

            return tempList;
        }
    }

    public void SetNodoA()
    {
        _graphCreator.SetNodoA(this);
    }

    public void SetNodoB()
    {
        _graphCreator.SetNodoB(this);
    }

    public bool IsConnected(Node _vecino)
    {
        bool aux = false;
        for (int i = 0; i < nodosVecinos.Count; i++)
        {
            if(nodosVecinos[i].nodoVecino == _vecino)
            {
                aux = true;
                break;
            }
        }
        return aux;
    }

    public void ActualizarDistancia()
    {
        for (int i = 0; i < nodosVecinos.Count; i++)
        {
            Vector3 posicionVecino = nodosVecinos[i].nodoVecino.transform.position;
            float distancia = Vector3.Distance(transform.position, posicionVecino);
            nodosVecinos[i].ActualizarDistancia(distancia);
        }
    }

    public void SetNodeAction(int _actionIndex)
    {       
        nodoAction = (NodoAction)_actionIndex;
        panelTransform.parent.gameObject.SetActive(false);
    }

    private void Start()
    {
        // Poblar el panel con las acciones del nodo
        int actionsLenght = Enum.GetNames(typeof(NodoAction)).Length;        
        Text textoBoton;
        Button boton;
        for (int i = 0; i < actionsLenght; i++)
        {
            GameObject actionObject = (GameObject)Instantiate(nodoActionBotonPrefab);

            actionObject.transform.parent = panelTransform;
            actionObject.GetComponent<RectTransform>().localScale = Vector3.one;
            
            textoBoton = actionObject.GetComponentInChildren<Text>();
            textoBoton.text = ((NodoAction)i).ToString();
            
            boton = actionObject.GetComponent<Button>();
            int index = new int();
            index = i;
            boton.onClick.AddListener(delegate { SetNodeAction(index); } ); 
        }


    }

    private void Update()
    {
        indexNodoText.text = indice.ToString();
    }
}
